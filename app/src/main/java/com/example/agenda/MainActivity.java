package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText txtNombre;
    private EditText txtTelefono1;
    private EditText txtTelefono2;
    private EditText txtDireccion;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Contacto saveContact;
    private long savedIndex = 1;
    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTelefono1 = (EditText) findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        txtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNota);
        cbFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btncerrar = (Button) findViewById(R.id.btnSalir);

        btncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().matches("") || txtTelefono1.getText().toString().matches("") ||
                        txtTelefono2.getText().toString().matches("") || txtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setDomicilio(txtDireccion.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setTelefono1(txtTelefono1.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setFavorito(cbFavorito.isChecked());
                    if(!isEdit){
                        nContacto.setID(savedIndex);
                        contactos.add(nContacto);
                        savedIndex++;
                    }else{
                        nContacto.setID(saveContact.getID());
                        editContacto(nContacto);
                    }

                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, list_activity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void editContacto(Contacto contacto)
    {
        for(int x = 0; x < contactos.size(); x++){
            if(contactos.get(x).getID() == contacto.getID()){
                this.contactos.set(x, contacto);
                break;
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();
            int typeAction = oBundle.getInt(getString(R.string.accion));
            this.contactos.clear();
            this.contactos.addAll((ArrayList<Contacto>) oBundle.getSerializable("list"));
            if(typeAction == 1){
                saveContact = (Contacto) oBundle.getSerializable("contactos");
                txtNombre.setText(saveContact.getNombre());
                txtTelefono1.setText(saveContact.getTelefono1());
                txtTelefono2.setText(saveContact.getTelefono2());
                txtDireccion.setText(saveContact.getDomicilio());
                txtNotas.setText(saveContact.getNotas());
                cbFavorito.setChecked(saveContact.isFavorito());
                isEdit = true;
            }
        }else{
            limpiar();
        }
    }

    public void limpiar() {
        txtNombre.setText("");
        txtDireccion.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        saveContact = null;
        isEdit = false;
        txtNombre.requestFocus();
    }
}