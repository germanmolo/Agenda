package com.example.agenda;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class list_activity extends AppCompatActivity {
    private TableLayout tblLista;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto> filter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_activity);

        tblLista = (TableLayout) findViewById(R.id.tbLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        this.filter = this.contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        TextView txtView1 = (TextView)findViewById(R.id.textview1);
        TextView txtView2 = (TextView)findViewById(R.id.textview2);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnList();
            }
        });
        cargarContactos();
    }

    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getNombre().contains(s))
                list.add(filter.get(x));
        }

        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    private void deleteContacto(long id)
    {
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getID() == id)
            {
                Log.e("MSG", "Se elimino");
                this.filter.remove(x);
                break;
            }
        }
        this.contactos = filter;
        tblLista.removeAllViews();
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        TextView txtView1 = (TextView)findViewById(R.id.textview1);
        TextView txtView2 = (TextView)findViewById(R.id.textview2);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnList();
            }
        });
        
//        txtView1.setText(R.string.nombre);
//        txtView1.setText(R.string.accver);
        cargarContactos();
    }
    public View.OnClickListener btnEliminarsAction(final long id)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ID", String.valueOf(id));
                deleteContacto(id);
            }
        };
    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size(); x++) {
            Contacto c = contactos.get(x);
            TableRow nRow = new TableRow(list_activity.this);

            TextView nText = new TextView(list_activity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(list_activity.this);
            nButton.setText(R.string.accion);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c= (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent ();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contactos", c);
                    oBundle.putSerializable("list", filter);
                    oBundle.putInt(getString(R.string.accion), 1);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);

            Button btnEliminar = new Button(list_activity.this);
            btnEliminar.setText("Eliminar");
            btnEliminar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnEliminar.setTextColor(Color.RED);
            btnEliminar.setOnClickListener(this.btnEliminarsAction(c.getID()));
            btnEliminar.setTag(R.string.contacto_g_index, c.getID());
            nRow.addView(btnEliminar);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            this.returnList();
        }
        return super.onKeyDown(keyCode, event);
    }


    private void returnList()
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", this.filter);
        bundle.putInt(getString(R.string.accion), 0);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
        Log.e("MSG", "Send Data");
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview ,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu); }
}
